﻿using Example.SpecFlowNUnit.PageObjects;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using TechTalk.SpecFlow;

namespace Example.SpecFlowNUnit.StepDefinitions
{
    [Binding]
    public class CreateGameSteps
    {
        #region Setup
        public static IWebDriver driver;
        [BeforeFeature]
        public static void BeforeFeature()
        {
            var options = new ChromeOptions();
            options.AddArgument("--ignore-ssl-errors=yes");
            options.AddArgument("--ignore-certificate-errors");
            driver = new ChromeDriver(options);
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            driver.Manage().Window.Maximize();
        }

        [AfterTestRun]
        public static void AfterTestRun()
        {
            driver.Quit();
        }
        #endregion

        [Given(@"I am on the homepage")]
        public void GivenIAmOnTheHomepage()
        {
            driver.Url = "https://localhost:5001/";
        }
        
        [When(@"I click on the create new game button")]
        public void WhenIClickOnTheCreateNewGameButton()
        {
            Homepage.createNewGameBtn.Click();
        }
        
        [When(@"I enter all mandatory fields and click save")]
        [Obsolete]
        public void WhenIEnterAllMandatoryFieldsAndClickSave()
        {
            var gameTitle = "Automated game " + DateTime.Now.ToString("HH:mm");
            CreateGamePage.gameTitleTxt.SendKeys(gameTitle);
            ScenarioContext.Current.Add("gameTitle", gameTitle);
            CreateGamePage.gameYearTxt.SendKeys(DateTime.Now.Year.ToString());
            CreateGamePage.gameMinPlayersTxt.SendKeys("2");
            CreateGamePage.gameMaxPlayersTxt.SendKeys("10");
            CreateGamePage.saveNewGameBtn.Click();
        }

        [Then(@"I am redirected to the overview page and the game is added")]
        [Obsolete]
        public void ThenIAmRedirectedToTheOverviewPageAndTheGameIsAdded()
        {
            Assert.AreEqual("Game overview", GameOverviewPage.overviewPageTitleLbl.Text);
            Assert.AreEqual(ScenarioContext.Current.Get<string>("gameTitle"), GameOverviewPage.firstEntry.Text);
        }
    }
}
