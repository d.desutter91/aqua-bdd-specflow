﻿using Example.SpecFlowNUnit.StepDefinitions;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Text;

namespace Example.SpecFlowNUnit.PageObjects
{
    class Homepage
    {
        public static IWebElement createNewGameBtn = CreateGameSteps.driver.FindElement(By.Id("createNewGame"));
        public static IWebElement gameOverviewBtn = CreateGameSteps.driver.FindElement(By.Id("gameOverview"));
    }
}
