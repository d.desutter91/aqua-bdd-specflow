﻿using Example.SpecFlowNUnit.StepDefinitions;
using OpenQA.Selenium;


namespace Example.SpecFlowNUnit.PageObjects
{
    class GameOverviewPage
    {
        public static IWebElement overviewPageTitleLbl = CreateGameSteps.driver.FindElement(By.XPath("/html/body/div[1]/main/h1"));
        public static IWebElement firstEntry = CreateGameSteps.driver.FindElement(By.XPath("/html/body/div[1]/main/table/tbody/tr[1]/td[1]"));
    }
}
