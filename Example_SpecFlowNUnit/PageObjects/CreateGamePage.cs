﻿using Example.SpecFlowNUnit.StepDefinitions;
using OpenQA.Selenium;

namespace Example.SpecFlowNUnit.PageObjects
{
    class CreateGamePage
    {
        public static IWebElement gameTitleTxt = CreateGameSteps.driver.FindElement(By.Id("Game_Title"));
        public static IWebElement gameYearTxt = CreateGameSteps.driver.FindElement(By.Id("Game_PublicationYear"));
        public static IWebElement gameMinPlayersTxt = CreateGameSteps.driver.FindElement(By.Id("Game_MinimumPlayers"));
        public static IWebElement gameMaxPlayersTxt = CreateGameSteps.driver.FindElement(By.Id("Game_MaximumPlayers"));
        public static IWebElement saveNewGameBtn = CreateGameSteps.driver.FindElement(By.Id("saveNewGame"));
    }
}
