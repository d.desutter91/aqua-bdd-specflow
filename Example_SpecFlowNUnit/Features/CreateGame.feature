Feature: Home
  In order to navigate quickly
  As a user
  I want to be able to create a new game and go to an overview of games

  Scenario: I create a new game
    Given I am on the homepage
    When I click on the create new game button
    And I enter all mandatory fields and click save
    Then I am redirected to the overview page and the game is added