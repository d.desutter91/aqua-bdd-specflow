using NUnit.Framework;
using Newtonsoft.Json.Linq;
using System.Net;
using RestSharp;
using System;

namespace APITest
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void CreateGame()
        {
                ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, errors) => true;
                var client = new RestClient("https://localhost:5001/api/Games");
                client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("application/json", "{\n\t\"title\": \"postman api game\",\n    \"publicationYear\": 2020,\n    \"minimumPlayers\": 1,\n    \"maximumPlayers\": 2\n}", ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);

                var jObject = JObject.Parse(response.Content);
                string id = jObject.GetValue("id").ToString();

                var client1 = new RestClient("https://localhost:5001/api/Games/" + id);
                client1.Timeout = -1;
                var request1 = new RestRequest(Method.GET);
                IRestResponse response1 = client1.Execute(request1);
                Assert.IsTrue(response.IsSuccessful);
            }
    }
}